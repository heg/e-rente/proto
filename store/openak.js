export const state = () => ({
  info: null,
  pension: null,
  insureds: {},
})

export const mutations = {
  saveInsured(state, data) {
    state.insureds[data.navs] = data.insured
  },
  setInfo(state, data) {
    state.info = data
  },
  setPension(state, data) {
    state.pension = data
  },
}
