export const state = () => ({
  registry: null,
  policies: null,
  selection: null,
})

export const mutations = {
  setRegistry(state, data) {
    state.registry = data
  },
  setPolicies(state, data) {
    state.policies = data
  },
  setSelection(state, data) {
    state.selection = data
  },
}
