export const state = () => ({
  login: null,
  fund_ids: null,
  ttl: null,
})

export const mutations = {
  setLogin(state, data) {
    state.login = data
  },
  setTTL(state, ttl) {
    state.ttl = Date.now() + 1000 * ttl
  },
  emptyLogin(state) {
    state.ttl = Date.now() + 2000
    state.login = null
  },
}
