export const state = () => ({
  registry: null,
  providents: null,
  selection: null,
  info: null,
})

export const mutations = {
  setRegistry(state, data) {
    state.registry = data
  },
  setProvidents(state, data) {
    state.providents = data
  },
  setSelection(state, data) {
    state.selection = data
  },
  setInfo(state, data) {
    state.info = data
  },
}
