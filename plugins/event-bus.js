import Vue from 'vue'

export default (context, inject) => {
  const eventBus = new Vue()
  inject('eventBus', eventBus)
}
// eventBus.install = function (Vue) {
//   Vue.prototype.$eventBus = new Vue()
// }

// Vue.use(eventBus)

/*
// Event emit
this.$eventBus.$emit('test-event')
or
app.$eventBus.$emit('test-event')

// Event Listen
this.$eventBus.$on('test-event', () => {})
 */
