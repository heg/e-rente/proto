import User from './models/user.js'
import Modals from './models/modals.js'
import OpenAK from './models/openak.js'
import OpenPK from './models/openpk.js'
import OpenPV from './models/openpv.js'
import Loader from './models/loader.js'
import Synthesis from './models/synthesis.js'

export default (context, inject) => {
  const models = {
    User: User(context),
    Modals: Modals(context),
    OpenAK: OpenAK(context),
    OpenPK: OpenPK(context),
    OpenPV: OpenPV(context),
    Loader: Loader(context),
    Synthesis: Synthesis(context),
  }

  inject('models', models)
}
