export default (context) => () => ({
  _emit(event) {
    const bus = context.app.$eventBus
    return bus.$emit(event)
  },
  _on(event, callback, action = 'on') {
    const bus = context.app.$eventBus
    return bus['$' + action](event, callback)
  },
  on() {
    return this._emit('loading:on')
  },
  off() {
    return this._emit('loading:off')
  },
  onOn(callback) {
    return this._on('loading:on', callback)
  },
  onOff(callback, action = 'on') {
    return this._on('loading:off', callback, action)
  },
})
