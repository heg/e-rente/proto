export default (context) => () => ({
  getRegistry() {
    return new Promise((resolve, reject) => {
      if (!context.store.state.openpv.registry) {
        const Api = context.app.$api
        return Api.get('/openpv/entities')
          .then((data) => {
            context.store.commit('openpv/setRegistry', data)
            resolve(data)
          })
          .catch(() => {
            reject(new Error('no registry'))
          })
      } else {
        resolve(context.store.state.openpv.registry)
      }
    })
  },
  getSelection() {
    return new Promise((resolve, reject) => {
      if (!context.store.state.openpv.selection) {
        const Api = context.app.$api
        return Api.get('/openpv/user/entities')
          .then((data) => {
            context.store.commit('openpv/setSelection', data)
            resolve(data)
          })
          .catch(() => {
            reject(new Error('no selection'))
          })
      } else {
        resolve(context.store.state.openpv.selection)
      }
    })
  },
  get() {
    return new Promise((resolve, reject) => {
      if (!context.store.state.openpv.info) {
        const Api = context.app.$api
        return Api.get('/open3k/info')
          .then((data) => {
            context.store.commit('openpv/setInfo', data)
            resolve(data)
          })
          .catch(() => {
            reject(new Error('no info'))
          })
      } else {
        resolve(context.store.state.openpv.info)
      }
    })
  },
  getProvident() {
    return new Promise((resolve, reject) => {
      if (!context.store.state.openpv.providents) {
        const Api = context.app.$api
        return Api.get('/openpv/user/providents')
          .then((data) => {
            context.store.commit('openpv/setProvidents', data)
            this.save(Object.values(data).pop()).then(() => {
              resolve(data)
            })
          })
          .catch(() => {
            reject(new Error('no provident'))
          })
      } else {
        resolve(context.store.state.openpv.providents)
      }
    })
  },
  save(info) {
    context.store.commit('openpv/setInfo', info)
    const Api = context.app.$api
    return Api.post('/open3k/info', info)
  },
  reset() {
    context.store.commit('openpv/setInfo', null)
    return new Promise((resolve, reject) => {
      this.get()
        .then((info) => {
          this.save({
            solde: null,
            rate: null,
            amount: null,
            paydate: info.paydate,
          }).finally(() => {
            resolve()
          })
        })
        .catch((err) => {
          reject(err)
        })
    })
  },
  saveSelection(ids) {
    context.store.commit('openpv/setProvidents', null)
    context.store.commit('openpv/setSelection', null)
    const Api = context.app.$api
    return Api.post('/openpv/user/entities', {
      ids,
    })
  },
})
