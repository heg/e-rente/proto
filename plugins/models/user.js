export default (context) => () => ({
  getMe(force) {
    return new Promise((resolve, reject) => {
      if (force || context.store.state.user.ttl < Date.now()) {
        const Api = context.app.$api
        return Api.get('/me')
          .then((data) => {
            context.store.commit('user/setLogin', data)
            context.store.commit('user/setTTL', 60 * 5)
            resolve(data)
          })
          .catch((data) => {
            context.store.commit('user/setTTL', 10)
            reject(data)
          })
      } else if (context.store.state.user.login) {
        resolve(context.store.state.user.login)
      } else {
        reject(new Error('not logged'))
      }
    })
  },
  _emit(event) {
    const bus = context.app.$eventBus
    return bus.$emit(event)
  },
  _on(event, callback, action = 'on') {
    const bus = context.app.$eventBus
    return bus['$' + action](event, callback)
  },
  emitIsConnected() {
    return this._emit('user-is-connected')
  },
  emitLoadedOnce() {
    return this._emit('me-loaded-once')
  },
  checkConnectionStatus() {
    return this._emit('check-if-connected')
  },
  onLoadedOnce(callback) {
    return this._on('me-loaded-once', callback)
  },
  onCheckConnectionStatus(callback, action = 'on') {
    return this._on('check-if-connected', callback, action)
  },
  onIsConnected(callback, action = 'once') {
    return this._on('user-is-connected', callback, action)
  },
})
