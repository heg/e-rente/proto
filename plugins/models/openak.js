export default (context) => () => ({
  saveInfo(form) {
    context.store.commit('openak/setInfo', null)
    context.store.commit('openak/setPension', null)
    const Api = context.app.$api
    return Api.post('/openak/pension-calculator/info', form)
  },
  getInfo() {
    return new Promise((resolve, reject) => {
      if (!context.store.state.openak.info) {
        const Api = context.app.$api
        return Api.get('/openak/pension-calculator/info')
          .then((data) => {
            context.store.commit('openak/setInfo', data)
            resolve(data)
          })
          .catch(() => {
            reject(new Error('no info'))
          })
      } else {
        resolve(Object.assign({}, context.store.state.openak.info))
      }
    })
  },
  getInsuredInfo(navs) {
    return new Promise((resolve, reject) => {
      if (!context.store.state.openak.insureds[navs]) {
        const Api = context.app.$api
        return Api.get(`/openak/insureds/${navs}`)
          .then((insured) => {
            context.store.commit('openak/saveInsured', {
              navs,
              insured,
            })
            resolve(insured)
          })
          .catch((err) => {
            reject(err)
          })
      } else {
        resolve(Object.assign({}, context.store.state.openak.insureds[navs]))
      }
    })
  },
  get(force = false) {
    return new Promise((resolve, reject) => {
      if (force || !context.store.state.openak.pension) {
        const Api = context.app.$api
        return Api.get('/openak/pension-calculator')
          .then((data) => {
            context.store.commit('openak/setPension', data)
            resolve(data)
          })
          .catch((err) => {
            reject(err)
          })
      } else {
        resolve(Object.assign({}, context.store.state.openak.pension))
      }
    })
  },
})
