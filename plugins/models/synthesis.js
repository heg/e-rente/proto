export default (context) => () => ({
  get(simul = null) {
    const Api = context.app.$api
    return Api.get('/synthesis/digest', simul)
  },
})
