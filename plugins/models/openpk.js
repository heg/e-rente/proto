const clone = function (object) {
  return JSON.parse(JSON.stringify(object))
}

export default (context) => () => ({
  reset() {
    return new Promise((resolve, reject) => {
      this.saveSelection([])
        .then(() => {
          return resolve()
        })
        .catch((err) => {
          reject(err)
        })
    })
  },
  deleteFund(fundId) {
    return new Promise((resolve, reject) => {
      this.getSelection()
        .then((selection) => {
          const tempSelection = clone(selection)
          const index = tempSelection.indexOf(fundId)
          if (index < 0) {
            return resolve()
          }
          tempSelection.splice(index, 1)
          this.saveSelection(tempSelection).then(() => {
            return resolve()
          })
        })
        .catch((err) => {
          reject(err)
        })
    })
  },
  getRegistry() {
    return new Promise((resolve, reject) => {
      if (!context.store.state.openpk.registry) {
        const Api = context.app.$api
        return Api.get('/openpk/pension-funds')
          .then((data) => {
            context.store.commit('openpk/setRegistry', data)
            resolve(data)
          })
          .catch(() => {
            reject(new Error('no registry'))
          })
      } else {
        resolve(context.store.state.openpk.registry)
      }
    })
  },
  getPolicy() {
    return new Promise((resolve, reject) => {
      if (!context.store.state.openpk.policies) {
        const Api = context.app.$api
        return Api.get('/openpk/user/pension-funds/policies')
          .then((data) => {
            context.store.commit('openpk/setPolicies', data)
            resolve(data)
          })
          .catch(() => {
            reject(new Error('no policy'))
          })
      } else {
        resolve(context.store.state.openpk.policies)
      }
    })
  },
  getSelection() {
    return new Promise((resolve, reject) => {
      if (!context.store.state.openpk.selection) {
        const Api = context.app.$api
        return Api.get('/openpk/user/pension-funds')
          .then((data) => {
            context.store.commit('openpk/setSelection', data)
            resolve(data)
          })
          .catch(() => {
            reject(new Error('no selection'))
          })
      } else {
        resolve(context.store.state.openpk.selection)
      }
    })
  },
  saveSelection(ids) {
    context.store.commit('openpk/setPolicies', null)
    context.store.commit('openpk/setSelection', null)
    const Api = context.app.$api
    return Api.post('/openpk/user/pension-funds', {
      ids,
    })
  },
})
