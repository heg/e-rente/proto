export default {
  commons: {
    warning_title: 'Information technique',
    disconnect: 'Déconnexion',
    reset: 'Effacer mes données',
    my_synthesis: 'Ma synthèse',
    sondage_button: 'Une remarque ?',
    sondage: 'eZZpUrCn',
    sondage_synthesis: 'F7u8Cvt4',
    from_age: 'à partir de ',
    from_date: 'à partir du ',
    gender: {
      MALE: 'Homme',
      FEMALE: 'Femme',
    },
    years_old: '0 | 1 an | {n} ans',
    avs_options: [
      {
        label: '756.0000.0000.02 - Alice',
        value: '7560000000002',
      },
      {
        label: '756.0000.0000.19 - Béatrice',
        value: '7560000000019',
      },
      {
        label: '756.0000.0000.26 - Conjoint de Béatrice',
        value: '7560000000026',
      },
      {
        label: '756.0000.0000.57 - Dan',
        value: '7560000000057',
      },
      {
        label: '756.0000.0000.64 - Conjointe de Dan',
        value: '7560000000064',
      },
    ],
    status: {
      just_married: {
        masc: 'Marié',
        fem: 'Mariée',
        neutral: 'Marié·e',
      },
      married: {
        masc: 'Marié / Partenariat enregistré',
        fem: 'Mariée / Partenariat enregistré',
        neutral: 'Marié·e / Partenariat enregistré',
      },
      bachelor: {
        masc: 'Célibataire',
        fem: 'Célibataire',
        neutral: 'Célibataire',
      },
      widow: {
        masc: 'Veuf',
        fem: 'Veuve',
        neutral: 'Veuf·ve',
      },
      divorced: {
        masc: 'Divorcé',
        fem: 'Divorcée',
        neutral: 'Divorcé·e',
      },
      remarried: {
        masc: 'Divorcé et remarié',
        fem: 'Divorcée et remariée',
        neutral: 'Divorcé·e et remarié·e',
      },
    },
    child: 'Sans enfant | 1 enfant | {n} enfants',
    where_to_find: 'Où le trouver ?',
    date_format: 'jj.mm.aaaa',
    date_at: 'le',
  },
  page: {
    about: {
      title: 'À propos',
      subtitle: 'Le service e-rentes.ch',
      content: `Le service e-rentes.ch a pour objectif de permettre aux personnes assurées de réunir les informations concernant leurs rentes et capitaux de vieillesse prévisionnels des trois piliers de manière automatique et en temps réel afin d’estimer les montants des prestations qui pourront être perçues à la retraite.
<br/><br/>


A ce jour, le présent prototype permet uniquement d'effectuer des simulations à partir de scénarios spécifiques, tels que la modification du taux d’activité ou la prise de retraite anticipée, appliqués sur des profils fictifs. A terme, la plateforme permettra des simulations individuelles à partir de situations réelles.
<br/><br/>


Le service e-rentes.ch a été créé et réalisé par un consortium d'entités publiques et privées : l’Office fédéral des assurances sociales (OFAS), la Centrale de compensation (CdC) et les deux éditeurs de logiciels Globaz SA et Neosis Solutions SA. Il est piloté et hébergé par la Haute école de gestion de Genève, et est co-financé par Innosuisse.`,
    },
    index: {
      logos: {
        ch: {
          alt: 'Logo de la Confederation Suisse',
          file: 'logos/swiss.png',
        },
        random: {
          file: 'logos/logorandom.png',
          alt: 'Logos',
        },
        hes: {
          alt: 'Logo de la HES',
          file: 'logos/HES.png',
        },
        heg: {
          alt: 'Logo de la HEG',
          file: 'logos/HEG.png',
        },
        globaz: {
          alt: 'Logo de Globaz',
          file: 'logos/globaz.png',
        },
        neosis: {
          alt: 'Logo de Neosis',
          file: 'logos/neosis.png',
        },
      },
      exemples: {
        title: `Exemple d'estimation`,
        users: [
          {
            photo: 'exemples/photo_2.png',
            name: 'Alice (59 ans)',
            info1: 'Célibataire',
            info2: 'Sans enfant',
            video: 'https://www.youtube.com/watch?v=qDbcMkDRK8w',
            export:
              'N4IgtgpiBcoM4FcBGMQEMEBcAWAGAPgGwBMAzAIxLECcAHACYBmaaxALLroY20hPeVwgANCAB2ASwDGAazFpIqOJjT0AVmhHiFUaCGWqNAAQDmYNBIA2AOikB7MFoAO0zAgBOukNkyYncaAB6QLhrE3c0ADc0FXdbB0ComLR3QI5aOghaAHYITlpcNAzsgFZiYjRGcgykckqSpDYAfjgAXjYCgDJ3VqcTTvpWnz84AFJSAEFR4gAxaZmpejFrDBxceLB5pNix2eVrJzETLQQnehj+AH0Y1GJcMgBacnIH3GyAFVxqaBK2aDJrCVcGwAFpaCDmKxKFTqNCmSE2eyOUQQiyWS6RCDuCSMCT8GCYdwICAAX1EUTgAEkxIw7DBQPJInBUKVCJx2RziFokBJ3DgACIXVDkaiEUivF73LRwCAAD1QMwAogBZCYAGUV3N5AouAGVCRIjqh7tZcORrCKxVpGXB9dijXpWab2c7OKauWSQJ5LDEJHYxHBsBInO8AJ5OXRiBCWSyib2+-2B4PM6AAbQAuqIpEHLPRPGIYBnRJg7CpLPyJCYIMp6SBzipC6AfXxLKh3qW0JYAAT0atd-OYPuQAPEmPVrTRSzEmAlCjrYi0UhscglMlNtAttsd7u9uBdqRoFwqBDy0ST6fQQSkThsQEkzN15Jc6BRmOeikVqs12CPhtp0BBmImCoAAB12TgpJgvI9ggXZmha5p3MQ2RaM2ECtnoABKEBARAXbDog6GWHhEwAGq6hOnYXuwFD3qI9asDAr6WJ6lhOE4n7VsBP4MY2IBoRhID6ikXYzAgYj0F2DxdthuH4ThhFjlogHcSAYEQXy0G9l2hAlF2aABpRU66Au2TUNYC5LiuHKcKQoiMOJ9CUvQba6u8TxmiAdG-oxL7RixohOJEnHfqAvH-vxG7oagup2LmeE4V2kSlp4XbMJiHioVFljPiApFYjKw6YPJI7oV2agIHAACOCCAJgEGBweazwWfcKFnlRuiCBy7XGc+i7WKQg1DcNw2iBItrmDGBJEqSD6IGA5juKGtYCagADCh4SMeso9nhkR4lYxFwDK4HuHYagQJgACXzI9ReV43ne9FPkx-miPNi2hs+zGek4cXSHiKagBCTiWHYoZFWqECYhhbL0RAUg+p49C6p2KTLZeBTsp6cASGA0YzHY7iOD+ADuW3YDAzCWDKKJgKD4OQ9D0XQHDIAQIwjAI1BmKCoOxpNYhrVaL2iMpPwqNIxj1QciSJJAA',
          },
        ],
      },
      video: {
        src: 'https://www.youtube.com/embed/h3YUbrRRQE4',
        title:
          "Le principe des trois piliers : vidéo explicative de l'Office fédéral des assurances sociales",
      },
      title: 'e-rentes.ch',
      subtitle:
        'J’estime ma retraite en ligne à partir de mes données personnelles',
      content: 'Authentification nécessaire et sécurisée par',
      action: 'Démarrer',
    },
    index2: {
      video: {
        src: 'https://www.youtube.com/embed/h3YUbrRRQE4',
        title:
          "Le principe des trois piliers : vidéo explicative de l'Office fédéral des assurances sociales",
      },
      title: 'Documents à préparer',
      subtitle: 'Données de connexion indispensables',
      action: 'J’ai toutes ces informations',
      tutos: [
        {
          title: 'Votre carte AVS ou d’assurance',
          html: `Vous aurez besoin de votre numéro AVS (pour vous et votre conjoint·e) type 756.0000.0000.00. Vous trouverez votre numéro AVS sur votre fiche de salaire ou sur votre carte d'assurance-maladie.`,
        },
        {
          title: 'Vos accès à votre caisse de pension 2ème pilier',
          html: `Il s’agit de vos données de connexion à la plateforme en ligne de votre caisse de pension : <i>nom de votre caisse</i>, <i>nom d'utilisateur</i> et <i>mot de passe</i>.`,
        },
        {
          title: 'Vos accès à votre compte 3ème pilier (3a)',
          html: `Si vous avez un compte de prévoyance 3ème pilier 3a de type bancaire (dépôt ou investi), vous aurez besoin de votre relevé de compte dans lequel vous trouverez le solde actuel de votre compte, le montant du capital de vieillesse prévisionnel, le taux d’intérêt prévisionnel et la date souhaitée de versement du capital.<br/><br/>

          Si vous avez un compte de prévoyance 3a assuranciel, vous aurez besoin du montant du capital (minimum ou garanti, selon votre police) ainsi que de votre police d’assurance.`,
        },
      ],
    },
    prelogin: {
      video: {
        src: 'https://www.youtube.com/embed/h3YUbrRRQE4',
        title:
          "Le principe des trois piliers : vidéo explicative de l'Office fédéral des assurances sociales",
      },
      title: 'Sécuriser ma connexion via SwissID',
      subtitle: 'Qu’est-ce que',
      content: `SwissID est un standard électronique suisse d'identité sécurisée permettant une authentification. Ce standard est soutenu par la Confédération et la Poste suisses.`,
      action: 'Me connecter',
      cgv: {
        part_1: 'Accepter les',
        part_2: 'Conditions d’utilisation de mes données',
        title: 'Conditions d’utilisation de mes données',
        html: `<p>
            TABLE DES MATIÈRES <br/>
<ul>
<li> Avertissements</li>
<li> Données personnelles (cas d’usages et données fictives)</li>
<li> Protection de l’utilisateur.trice et de ses données personnelles</li>
<li> Limitation de garantie et de responsabilité lors de l’utilisation de e-rentes.ch</li>
<li> Protection des données des personnes mariées et des partenaires enregistré·es</li>
<li> Modification des conditions</li>
</ul><br/>
<br/>
<strong>Avertissements</strong> <br/>
<br/>
e-rentes.ch est un service qui permet d'effectuer une estimation en ligne des rentes et capitaux de vieillesse.  <br/>
<br/>
Le résultat est obtenu par le biais de procédures simplifiées et ne représente aucune garantie. La présence d'éléments hypothétiques dans le calcul implique que les montants annoncés le sont à titre indicatif. Ces montants n'ont par conséquent aucune valeur juridique et n'engagent en aucun cas les différents organismes avec lesquels e-rentes.ch échange pour restituer les estimations. De plus, l'évaluation des rentes et capitaux de vieillesse ne donne droit à aucune prestation spécifique.  <br/>
<br/>
Si vous êtes ou avez été au bénéfice d'une rente de survivant ou d'invalidité, le résultat de l'évaluation des montants de vos rentes et capitaux de vieillesse sera d’autant moins fiable. <br/><br/>
<br/>
<strong>Données personnelles (cas d’usages et données fictives)</strong>  <br/><br/>

Le service numérique e-rentes.ch ne permet à ce jour que d’effectuer des estimations et simulations de calculs des rentes et capitaux de vieillesse pour des cas d’usage bien précis car il s'agit d’un prototype établi à partir de profils et de situations fictifs. <br/><br/><strong>Les données de navigation et les données personnelles (fictives) auxquelles e-rentes.ch fait appel dans le cadre de l’estimation de rentes et capitaux de vieillesse sont stockées durant 60 minutes et sont ensuite supprimées. </strong><br/>
<br/>
Seules sont saisies et remontées sur e-rentes.ch les données nécessaires à l’estimation des rentes et capitaux de vieillesse.  <br/>
<br/><br/>
<strong>Protection de l’utilisateur.trice et de ses données personnelles</strong>
<br/>
<br/>
 e-rentes.ch s'interdit d'accéder au contenu des données saisies en ligne par l'utilisateur.trice tant que celles-ci ne lui ont pas été volontairement transmises. En outre, e-rentes.ch s’engage à prendre toutes les mesures nécessaires permettant de garantir la sécurité et la confidentialité des informations fournies par l’utilisateur.trice, et plus spécifiquement :  <br/><br/>- e-rentes.ch adopte une politique "Do Not Track" (DNT). Lorsque vous visitez notre service, nos serveurs Web <strong>n’enregistrent AUCUNE activité ou donnée dans le fichier journal.</strong>   <br/>
- e-rentes.ch est soumis à loi sur la protection des données <strong><a target=_blank" href="https://www.kmu.admin.ch/kmu/fr/home/faits-et-tendances/digitalisation/protection-des-donnees/nouvelle-loi-sur-la-protection-des-donnees-nlpd.html"></a> qui prévoit que les données personnelles doivent être protégées contre tout traitement illicite par des mesures organisationnelles et techniques appropriées.</strong> <br/>
<br/>
<br/>
<strong>Limitation de garantie et de responsabilité lors de l’utilisation de e-rentes.ch</strong><br/>
<br/>
e-rentes.ch prend toutes les mesures adéquates et usuelles pour assurer l’intégrité, la disponibilité et la confidentialité des données qu'il met à disposition des utilisateur.trices ou des visiteur.euses, ou qu'il leur transmet, ainsi que pour se prémunir des programmes malveillants sur son réseau.  <br/>
<br/>
e-rentes.ch décline ainsi toute responsabilité pour d’éventuelles atteintes, directes ou indirectes, de quelque nature que ce soit à la confidentialité ou à l’intégrité des données intervenant à l’extérieur de son réseau informatique. <br/><br/>
<br/>
<strong>Protection des données des personnes mariées et des partenaires enregistré·es</strong>  <br/>
<br/>
Les personnes mariées, ou liées par un partenariat enregistré, font l’objet d’une estimation des rentes et capitaux de vieillesse partiellement commune. Dans ces cas, chaque personne a accès aux données communes pour une estimation individuelle des prestations. <br/><br/>Afin que les estimations restituées par e-rentes.ch soient les plus précises possible, il est impératif que vous ayez informé les divers organismes responsables de la gestion de vos prestations de vieillesse de tout changement dans votre situation personnelle et professionnelle, de manière à ce que les données vous concernant soient à jour par rapport à votre situation réelle. <br/><br/>
En outre, ces simulations ne tiennent pas compte des ajustements relatifs à l’évolution de la conjoncture économique et sociale entre le jour où celles-ci sont effectuées et le moment où vous percevrez vos prestations de vieillesse.

<br/><br/>
<strong>Modification des conditions</strong>  <br/>
<br/>
Le prototype se réserve le droit de modifier les présentes conditions d'utilisation après consultation avec les partenaires publics et privés. Les utilisateur.trices seront informé.es des changements et devront accepter les nouvelles conditions pour continuer d'utiliser la plateforme.
          </p>`,
      },
    },
    avs: {
      video: {
        src: 'https://www.youtube.com/embed/h3YUbrRRQE4',
        title:
          "Le principe des trois piliers : vidéo explicative de l'Office fédéral des assurances sociales",
      },
      title:
        'Estimer ma rente de vieillesse étatique <br />(LAVS - 1er pilier)',
      action: 'Estimer ma rente de vieillesse étatique',
      warning: {
        body: 'Les cas autres que <q>Célibataire</q> et <q>Marié·e</q> ne sont pas encore traités par le prototype',
      },
      modal: {
        title: 'Où trouver votre nºAVS',
        content: `Vous trouverez votre numéro AVS sur votre certificat d’assurance AVS-AI ou sur votre carte suisse d’assurance-maladie. Il s’agit d’un document physique au format carte de crédit, que vous êtes supposé.e conserver en permanence avec vous. Celui-ci figure également sur vos fiches de salaire.
            <br /><br />
            Votre numéro AVS est du type
            756.0000.0000.00`,
      },
      form: {
        labels: {
          avs: 'N° AVS',
          avs_neutral: 'N° AVS',
          avs_info: '',
          birthdate: 'Date de naissance',
          gender: 'Genre',
          status: 'État civil',
          age: 'Âge',
          relationship_start: 'Date du mariage',
          relationship_end: 'Date de fin de mariage',
          partner_avs: 'N° AVS du / de la partenaire',
          partner_death: 'Date de décès du / de la partenaire',
          partner_birth: 'Date de naissance du / de la partenaire',
          partner_age: 'Âge du / de la partenaire',
          partner_gender: 'Genre du / de la partenaire',
          child: 'Enfant',
          child_count: "Nombre d'enfants",
          child_birth: "Date de naissance de l'enfant",
          child_death: "Date de décès de l'enfant",
          child_is_alive: 'Vivant',
        },
      },
    },
    lpp: {
      video: {
        src: 'https://www.youtube.com/embed/h3YUbrRRQE4',
        title:
          "Le principe des trois piliers : vidéo explicative de l'Office fédéral des assurances sociales",
      },
      title:
        'Estimer ma rente vieillesse professionnelle <br />(LPP - 2ème pilier)',
      action: 'Estimer ma rente de vieillesse professionnelle',
      modal: {
        title: 'Votre caisse de pension',
        content: `Chaque année au mois de janvier, vous recevez par courrier un document appelé “<strong>Attestation&nbsp;de&nbsp;prévoyance</strong>”. Le nom de votre caisse de pension y est indiqué. <br/>Votre employeur peut également vous le fournir sur demande.`,
      },
      form: {
        label: 'Sélectionner votre caisse de pension / organisme de prévoyance',
      },
    },
    '3a': {
      video: {
        src: 'https://www.youtube.com/embed/lpiMQkzc_RE?si=A7Xf-pvD5bYb-pTI',
        title:
          "Le principe des trois piliers : vidéo explicative de l'Office fédéral des assurances sociales",
      },
      title: 'Estimer ma prévoyance privée <br />(3a - 3ème pilier)',
      action: 'Estimer ma prévoyance privée',
      modal: {
        title: 'Votre banque',
        content: `lorem ipsum`,
        solde: {
          title: 'Solde actuel du compte',
          content: `Vous trouverez cette information sur votre “Attestation fiscale
              annuelle du 3ème pilier” (3a) que vous recevez en format papier
              entre fin janvier et début février.<br /><br />

              e-rentes.ch est capable de traiter deux situations de 3ème pilier : 
 
              <ul>
<li>- 3a placé</li>
<li>- 3a non placé </li></ul>
`,
        },
        amount: {
          title: 'Versements annuels prévisionnels',
          content: `Il s’agit des versements annuels que vous souhaitez effectuer dans le futur jusqu’à l’âge de la retraite sur votre compte de 3ème pilier (3a). <br/><br/>
Si vous êtes salarié·e, vous pouvez déduire le montant maximal de versement autorisé pour l'année en cours, soit CHF 7'056.- (plafond pour 2023). Si vous êtes indépendant·e et que vous n'avez pas de 2ème pilier, vous pouvez déduire jusqu'à 20 % de votre revenu, sans toutefois dépasser CHF 35'280.- par an (plafond pour 2023).
`,
        },
        rate: {
          title: null,
          content: null,
        },
      },
      form: {
        label: 'Sélectionner votre banque',
        solde: 'Solde actuel du compte',
        amount: 'Montant annuel des versements',
        rate: 'Taux d’intérêt prévisionnel',
        paydate: 'Date souhaitée de versement du capital',
      },
    },
    export: {
      title: 'e-rentes.ch :<br/>estimation rente de vieillesse',
      warning: `e-rentes.ch est un service qui permet d'effectuer une estimation en ligne des rentes et capitaux de vieillesse. Le résultat est obtenu par le biais de procédures simplifiées et ne représente aucune garantie. La présence d'éléments hypothétiques dans le calcul implique que les montants annoncés le sont à titre indicatif. Ces montants n'ont par conséquent aucune valeur juridique et n'engagent en aucun cas les différents organismes avec lesquels e-rentes.ch échange pour restituer les estimations. De plus, l'évaluation des rentes et capitaux de vieillesse ne donne droit à aucune prestation spécifique.`,
    },
    synthesis: {
      title: 'Mes données',
      warning: `e-rentes.ch est un service qui permet d'effectuer une estimation en ligne des rentes et capitaux de vieillesse. Le résultat est obtenu par le biais de procédures simplifiées et ne représente aucune garantie. La présence d'éléments hypothétiques dans le calcul implique que les montants annoncés le sont à titre indicatif. Ces montants n'ont par conséquent aucune valeur juridique et n'engagent en aucun cas les différents organismes avec lesquels e-rentes.ch échange pour restituer les estimations. De plus, l'évaluation des rentes et capitaux de vieillesse ne donne droit à aucune prestation spécifique.`,
      actions: {
        modify_personal_data:
          'Estimer à nouveau ma rente de vieillesse étatique ',
        simulate_data_changes:
          'Effectuer des simulations de changements de situation',
      },
      without_change: 'Sans<br/> changement',
      with_change: 'Avec<br/> changement',
      parts: {
        total: {
          title: 'Rentes & capitaux prévisionnels de vieillesse en CHF',
          annuity_label: 'Total des rentes mensuelles',
          capital_label: 'Total des capitaux',
        },
        lavs: {
          title:
            'Rente de vieillesse étatique <small>(LAVS - 1er pilier)</small> en CHF',
          from: 'du',
          to: 'au',
          annuity_avs_label: 'Rente mensuelle AVS',
          partner_annuity_from:
            'Votre conjoint·e touche une rente de vieillesse étatique dès le',
        },
        lpp: {
          title:
            'Rente de vieillesse professionnelle <small>(LPP - 2ème pilier)</small> en CHF',
          action_label: 'Pour estimer votre rente',
          action: ' Accéder à votre caisse de prévoyance professionnelle',
          annuity_label: 'Rente mensuelle',
        },
        '3a': {
          title: 'Prévoyance privée <small>(3ème pilier - 3a)</small> en CHF',
          action_label: 'Pour estimer votre prévoyance privée',
          action: 'Ajouter vos informations',
          capital_label: 'Solde en votre faveur',
          capital_until: 'Versement mensuel jusqu’au',
          capital_projection_label: 'Capitaux de vieillesse projetés',
          capital_projection_from: 'à partir du',
        },
      },
      modals: {
        simulate: {
          intro:
            'Tout changement des informations saisies entraînera une modification des simulations délivrées.',
          title: 'Changements des données',
          form: {
            salary: 'Salaire brut futur',
            rate: 'Taux d’activité',
            date: 'Date de départ à la retraite',
            action: 'Lancer la simulation',
            outro: `En ce qui concerne l'AVS, il est possible d'anticiper sa retraite d’un ou deux ans avant l'âge légal ou au contraire, de la reporter d'une durée pouvant aller jusqu'à 5 ans.<br/><br/>


Une même souplesse est applicable pour ce qui relève du 2ème pilier, mais son ampleur dépend des règlements de votre Caisse de Pension.`,
          },
        },
        total: {
          title: 'Rentes & capitaux de vieillesse prévisionnels : précisions',
          content: `La première partie de la synthèse est un résumé des trois sections suivantes et indique l’estimation :
          <ul><li>- de la rente mensuelle du 1er pilier additionnée à celle de la rente mensuelle du 2ème pilier</li>
          <li>- et des éventuels capitaux de la prévoyance professionnelle (2ème pilier) et de la prévoyance privée (3a)</li></ul>`,
        },
        avs: {
          title: 'Rente de vieillesse étatique : précisions',
          content: `Le montant indiqué correspond à une estimation de la rente de vieillesse que vous percevrez mensuellement dès 65 ans pour ce qui relève du 1er pilier.<br/>
Attention, si vous êtes marié·e, ce montant sera modifié dès que votre conjoint·e aura elle/lui aussi atteint l’âge de la retraite.`,
        },
        lpp: {
          title: 'Rente de vieillesse professionnelle : précisions',
          content: `Le montant indiqué correspond à une estimation de la rente que vous percevrez mensuellement dès 65 ans, pour ce qui relève du 2ème pilier.`,
        },
        '3a': {
          title: 'Rente de prévoyance privée : précisions',
          content: `e-rentes.ch est capable de traiter deux situations de 3ème pilier :<br/>
          <ul>
<li>- 3a placé</li>
<li>- 3a non placé</li></ul>`,
        },
      },
    },
  },
}
