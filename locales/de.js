export default {
  commons: {
    warning_title: 'Technische Information',
    disconnect: 'Abmeldung',
    reset: 'Meine Daten löschen',
    my_synthesis: 'Meine Synthese',
    sondage_button: 'Eine Bemerkung ?',
    sondage: 'do9vOCwf',
    sondage_synthesis: 'opTaljBr',
    from_age: 'ab ',
    from_date: 'ab ',
    gender: {
      MALE: 'Mann',
      FEMALE: 'Frau',
    },
    years_old: '0 | 1 Jahr | {n} Jahre',
    avs_options: [
      {
        label: '756.0000.0000.02 - Alice',
        value: '7560000000002',
      },
      {
        label: '756.0000.0000.19 - Beatrice',
        value: '7560000000019',
      },
      {
        label: '756.0000.0000.26 - Ehepartner von Beatrice',
        value: '7560000000026',
      },
      {
        label: '756.0000.0000.57 - Dan',
        value: '7560000000057',
      },
      {
        label: '756.0000.0000.64 - Ehepartnerin von Dan',
        value: '7560000000064',
      },
    ],
    status: {
      just_married: {
        masc: 'Verheiratet',
        fem: 'Verheiratet',
        neutral: 'Verheiratet',
      },
      married: {
        masc: 'Verheiratet',
        fem: 'Verheiratet',
        neutral: 'Verheiratet',
      },
      bachelor: {
        masc: 'Single',
        fem: 'Single',
        neutral: 'Single',
      },
      widow: {
        masc: 'Verwitwet',
        fem: 'Verwitwet',
        neutral: 'Verwitwet',
      },
      divorced: {
        masc: 'Geschieden',
        fem: 'Geschieden',
        neutral: 'Geschieden',
      },
      remarried: {
        masc: 'Geschieden und wieder verheiratet',
        fem: 'Geschieden und wieder verheiratet',
        neutral: 'Geschieden und wieder verheiratet',
      },
    },
    child: 'Ohne Kind | 1 Kind| {n} Kinder',
    where_to_find: 'Wo finden Sie das?',
    date_format: 'tt.mm.jjjj',
    date_at: 'am',
  },
  page: {
    about: {
      title: 'Über',
      subtitle: 'Der Dienst e-rentes.ch.',
      content: `Der Zweck des Dienstes e-rentes.ch besteht darin, den versicherten Personen die Möglichkeit zu geben, automatisch und in Echtzeit Informationen zu ihren Renten und dem voraussichtlichen Altersvorsorgekapital aus den drei Säulen einzuholen, um ihr Alterseinkommen abzuschätzen.
<br/><br/>
Bisher ist der vorliegende Prototyp auf Anwendungsfällen aufgebaut und ermöglicht lediglich Simulationen anhand spezifischer Szenarien, wie z. B. Änderung des Beschäftigungsgrades oder Frühverrentung, die auf fiktive Profile angewendet werden. Künftig wird die Plattform individuelle Simulationen basierend auf realen Situationen ermöglichen.
<br/><br/>
Die Entwicklung des Dienstes e-rentes.ch wurde von Innosuisse mitfinanziert und in Zusammenarbeit mit dem Bundesamt für Sozialversicherungen (OFAS), der Zentralen Ausgleichskasse (CdC) und den beiden Softwareherstellern Globaz SA und Neosis Solutions SA durchgeführt.`,
    },
    index: {
      logos: {
        random: {
          file: 'logos/logorandom_de.png',
        },
      },
      exemples: {
        title: `Schätzungsbeispiel`,
        users: [
          {
            photo: 'exemples/photo_0.png',
            name: 'Beatrice (40 Jahre)',
            info1: 'Verheiratet',
            info2: '1 Kind',
            video: 'https://www.youtube.com/watch?v=qDbcMkDRK8w',
            export:
              'N4IgtgpiBcoM4FcBGMQHMD2G0BsIFoMBDBAFwAsAmAHwEYAGADmYDYAWFgThcrYHYArPUqdalHiAA06AJYA3CADsA+oqKRUAFXJE1cGXCkgAZupk4AnqvVRoIAAoAnGcdIGjimQGMA1mo12FLpE+nAAdAAOzq7u0v62INrBoQAETi5uhtIR3qQIjgnkpKQRcNAA9OU45ADMYZjYeAhwEI5eGIqkSqRh7WDlROUAgkNeABKapAAm5ADiAJL0AGKz+AAaALIAIgAyAB5jcBtyaADqPvYA8mPGa-MAaso4GGMsygCOs8oA7uSOPgAtR4AYUYAF44Nx8F4jM8vEQ8KglPhZgAhIwICJTIhdKbKHGoSjCGr4eicUksTS0GrQWh8aD0PhhNi0FgAowQMBEcyoIJ6AyRaKZAACaC55l6GDAHPFOGUCmiMggUxgpEcCAgAF9pEQ5HB5opjBgYKA1HrUIIWPRrTbrbROEYkDJHBQtjiEvbGJRSd76LQjC09qglgBRDZDHYhx3O13ugDKapkijQqD9YWEYU9lA8urgCecyYtAhY6ZtpetmYd2pABRwOJkHTg5BkEU0FgitkUCBwOGktfrjebpRgAG14KQiC7CX76D78LR-dJSO2EnGrgBVONR7KtOAdE0gM2GOyW2228TRl3kN1dVCeknCUlsHNuDoImTLmAMeh9iD6KZKF4Fhfta0iBqg4aRpesZdPmSYpnY9BsOmlCVowNQgNWcATi6cGFnYC4oWERIMJhAC60heM2OBTAUiijqATpXjeCQkXwpLkqR0hMTBEB4QhIAMOmnDEX6fBGAYQw4PIthqhqmoUSApAYBOOBbDIaB-qQB7YhODEgHWSAQDgqCzH+6hdCkYBvqQ0lURAKQAErdEoRhyAiGowCIbCMGE3AsAwrIsMFLB8NqoCGcZqBSV0jhwD4RA5KpbkebYNQ1PQNTIQICnSLpRDZtAXY9tWubqZp2E6TiRD6c2nSoHIHQpGmmWifwKROnAzW0OmdREj5sJEEZJl2E42CKBg2EyAAXkqLoOdZai2d45AOUM9xjPgzmdA5SYpMCYxLClOCebSnA1DlkgRUNUV2PMfwpCGq0RJOpCKK0HUQLNEBFCkEBJg52FEDidmrSkMW7nRllDSkAFgN1vVtYwmFXSAdXaXYMMtX1SHI9IkUjQ4jjjZNbjffNVk2aDa0bVtLkpHtB1HdI7knbYdLcLlID5YVxU4NWOARBE5VaVVenQGOBk3YTCaTikSwIIoUwpPgaTE2gE1TeTlmLSDK0Odtt7SOjqAwywAgpAAUkQfxQCzqUgdSfnBYFIUhYw0jGIrUzzCqdiaHGmjzn65F5dVvPdvz2RyCLlWwNz1X6QTplkDoRmKCk00ICk910SksyK9hrn49LhUgBsVP6ykAI2ydyYdQYmcIPD2NteJ9tsyBNodxqhXoWE6WD0Pw-SAYcZcj2qrqlqimIGAXKOMB8fJ6N6ua2Tc2WRDcUJUlCLHad6WZdl1Y8zAfNgc3C8WBHJXZBgdlKseoCchEzwWJAnQ7BACgjVaeUQC8HWAoUw4wIknEvPgRJrRYRkGAbsSwMCOGlPHb4H5yAwFMDgFo0hX7v0-qQb+v8YD-xABAYwxhAFuAUCxVMPVWr9XbtzQBwDlRgOAZA6B9BNSaiAA',
          },
        ],
      },
      video: {
        src: 'https://www.youtube.com/embed/n61WfLTW1IU?si=9DQllfCL3udT_xfp',
        title:
          'Das Drei-Säulen-Prinzip: Erklärendes Video vom Bundesamt für Sozialversicherung',
      },
      title: 'e-rentes.ch',
      subtitle:
        'Ich schätze meine Rente online anhand meiner persönlichen Daten.',
      content: 'Authentifizierung erforderlich und durch SwissID gesichert',
      action: 'Starten',
    },
    index2: {
      video: {
        src: 'https://www.youtube.com/embed/n61WfLTW1IU?si=9DQllfCL3udT_xfp',
        title:
          'Das Drei-Säulen-Prinzip: Erklärendes Video vom Bundesamt für Sozialversicherung',
      },
      title: 'Vorzubereitende Dokumente',
      subtitle: 'Unverzichtbare Anmeldedaten',
      action: 'Ich habe alle diese Informationen',
      tutos: [
        {
          title: 'Ihre AHV- oder Krankenversicherungskarte',
          html: `Sie benötigen Ihre AHV-Nummer vom Typ 756.0000.0000.00. Ihre AHV-Nummer finden Sie auf Ihrer Lohnabrechnung oder auf Ihrer Krankenversicherungskarte.`,
        },
        {
          title: 'Ihr Zugang zu Ihrer Pensionskasse der 2. Säule ',
          html: 'Dies sind Ihre Verbindungsdaten zur Online-Plattform Ihrer Pensionskasse: Name Ihrer Kasse, Benutzername und Passwort.',
        },
        {
          title: 'Ihr Zugang zum Ihrem Konto der 3. Säule (3a)',
          html: `Wenn Sie über ein Vorsorgekonto der dritten Säule 3a in Form eines Bankkontos (Einlage oder Anlage) verfügen, benötigen Sie Ihren Kontoauszug, auf dem Sie den aktuellen Kontostand finden. Wenn Sie ein versicherungsbasiertes 3a-Vorsorgekonto haben, benötigen Sie die Höhe des Kapitals (Mindest- oder Garantiekapital, je nach Police) sowie Ihre Versicherungspolice.
  Schaltfläche «Ich habe alle Daten».`,
        },
      ],
    },
    prelogin: {
      video: {
        src: 'https://www.youtube.com/embed/n61WfLTW1IU?si=9DQllfCL3udT_xfp',
        title:
          'Das Drei-Säulen-Prinzip: Erklärendes Video vom Bundesamt für Sozialversicherung',
      },
      title: 'Ich sichere meine Verbindung über SwissID',
      subtitle: 'Was ist ',
      content: `SwissID ist ein Schweizer elektronischer Standard für sichere Identität, der eine Authentifizierung ermöglicht. Dieser Standard wird von der Schweizerischen Eidgenossenschaft und der Schweizerischen Post unterstützt.`,
      action: 'Einloggen',
      cgv: {
        part_1: 'Ich akzeptiere die Nutzungsbedingungen meiner Daten',
        part_2: 'Bedingungen zur Verwendung der Plattform e-rentes.ch',
        title: 'Nutzungsbedingungen meiner Daten',
        html: `<p>
            INHALTSVERZEICHNIS <br/>
<ul>
<li> Warnhinweise</li>
<li> Personenbezogene Daten (Anwendungsfälle und fiktive Daten)</li>
<li> Schutz des Nutzers/der Nutzerin und seiner/ihrer personenbezogenen Daten</li>
<li> Gewährleistungs- und Haftungsbeschränkung bei der Nutzung von e-rentes.ch</li>
<li> Datenschutz für verheiratete Personen und eingetragene Partner</li>
<li> Änderungen der Bedingungen</li>
</ul><br/>
<br/>
<strong>Warnhinweise</strong> <br/>
<br/>
e-rentes.ch ist ein Dienst, der es Ihnen ermöglicht, online eine Schätzung der Altersrente vorzunehmen.<br/>
<br/>
Das Ergebnis wird durch vereinfachte Verfahren ermittelt und stellt keine Garantie dar. Das Vorhandensein hypothetischer Elemente in der Berechnung impliziert, dass es sich bei den angegebenen Beträgen um Richtwerte handelt. Diese Beträge haben daher keinen rechtlichen Wert und verpflichten die verschiedenen Organisationen, mit denen e-rentes.ch austauscht, in keiner Weise zur Wiederherstellung der Schätzungen. Darüber hinaus ergibt sich aus der Rentenbemessung kein Anspruch auf eine bestimmte Leistung. <br/>
<br/>
Wenn Sie eine Hinterbliebenen- oder Invalidenrente beziehen oder bezogen haben, ist das Ergebnis der Bemessung Ihrer Altersrente umso weniger verlässlich. <br/><br/>
<br/>

<strong>Personenbezogene Daten (Anwendungsfälle und fiktive Daten)</strong>  <br/><br/>
Bisher können mit dem digitalen Dienst e-rentes.ch nur Schätzungen und Simulationen zur Berechnung von Altersrenten durchgeführt werden, da es sich um einen Prototyp handelt, der anhand von fiktiven Profilen und Situationen erstellt wurde.</strong><br/>
<br/>
Die Navigationsdaten und (fiktiven) personenbezogenen Daten, auf die e-rentes.ch im Rahmen der Schätzung von Altersrenten zurückgreift, werden 60 Minuten lang gespeichert und danach gelöscht.  Es werden nur die für die Schätzung der Altersrenten und Kapitalbeträge notwendigen Daten erfasst und auf e-rentes.ch hochgeladen.<br/>
<br/><br/>

<strong>Schutz des Nutzers/der Nutzerin und seiner/ihrer personenbezogenen Daten</strong>
<br/>
<br/>
 Es ist e-rentes.ch untersagt, auf den Inhalt der vom Nutzer/der Nutzerin online eingegebenen Daten zuzugreifen, solange diese nicht freiwillig übermittelt wurden. Darüber hinaus verpflichtet sich e-rentes.ch alle notwendigen Massnahmen zu ergreifen, um die Sicherheit und Vertraulichkeit der vom Nutzer/der Nutzerin bereitgestellten Informationen zu gewährleisten, insbesondere:
 <br/>
- e-rentes.ch wendet eine „Do Not Track“-Richtlinie (DNT) an. <strong> Wenn Sie unseren Service besuchen, zeichnen unsere Webserver KEINE Aktivitäten oder Daten in der Protokolldatei auf.</strong>   <br/>
- e-rentes.ch unterliegt dem Datenschutzgesetz <strong> DSG <a target=_blank" href="https://www.kmu.admin.ch/kmu/de/home/fakten-trends/digitalisierung/datenschutz/neues-datenschutzgesetz-rev-dsg.html"></a>das vorschreibt, dass personenbezogene Daten durch geeignete organisatorische und technische Maßnahmen vor rechtswidriger Verarbeitung geschützt werden müssen.</strong>
 <br/><br/>
<br/>
<strong>Gewährleistungs- und Haftungsbeschränkung bei der Nutzung von e-rentes.ch</strong><br/>
<br/>
e-rentes.ch ergreift alle geeigneten und üblichen Massnahmen, um die Integrität, Verfügbarkeit und Vertraulichkeit der Daten zu gewährleisten, die es Nutzern/Nutzerinnen oder Besuchern/Besucherinnen zur Verfügung stellt oder an diese übermittelt, sowie um sich vor Malware in seinem Netzwerk zu schützen. <br/>
<br/>
e-rentes.ch lehnt daher jede Verantwortung für direkte oder indirekte Verletzungen der Vertraulichkeit oder Integrität von Daten jeglicher Art ab, die ausserhalb ihres Informatiknetzwerks auftreten. <br/><br/>
<br/>

<strong>Datenschutz für verheiratete Personen und eingetragene Partner</strong>  <br/>
<br/>
Für verheiratete Personen oder Personen, die in einer eingetragenen Partnerschaft leben, wird eine teilweise gemeinsame Rentenschätzung vorgenommen. In diesen Fällen hat jede Person Zugang zu den gemeinsamen Daten für eine individuelle Rentenschätzung. <br/>
<br/><br/>
<strong>Änderungen der Bedingungen</strong>  <br/>
<br/>
Die Plattform behält sich das Recht vor, diese Nutzungsbedingungen nach Rücksprache mit öffentlichen und privaten Partnern zu ändern. Nutzer/Nutzerinnen werden über die Änderungen informiert und müssen die neuen Bedingungen akzeptieren, um die Plattform weiterhin nutzen zu können.
          </p>`,
      },
    },
    avs: {
      video: {
        src: 'https://www.youtube.com/embed/n61WfLTW1IU?si=9DQllfCL3udT_xfp',
        title:
          'Das Drei-Säulen-Prinzip: Erklärendes Video vom Bundesamt für Sozialversicherung',
      },
      title: 'Schätzen meiner staatlichen Altersrente <br />(AHVG –1. Säule)',
      action: 'Schätzen meiner staatlichen Altersrente ',
      warning: {
        body: 'Andere Fälle als <q>Single</q> und <q>Verheiratet</q> werden vom Prototyp noch nicht behandelt',
      },
      modal: {
        title: 'Wo finden Sie Ihre AHV-Nummer?',
        content: ` Ihre AHV-Nummer finden Sie auf Ihrem AHV-IV-Versicherungsausweis oder auf Ihrer Schweizer Krankenversicherungskarte. Dabei handelt es sich um ein physisches Dokument im Kreditkartenformat, das Sie stets bei sich tragen sollten. Sie wird auch auf Ihren Gehaltsabrechnungen ausgewiesen.
            <br /><br />
            Ihre AHV-Nummer ist vom Typ
            756.0000.0000.00`,
      },
      form: {
        labels: {
          avs: 'AHV-Nummer',
          avs_neutral: 'AHV-Nummer',
          avs_info: '',
          birthdate: 'Geburtsdatum',
          gender: 'Genre',
          status: 'Zivilstand',
          age: 'Alter',
          relationship_start: 'Datum der Hochzeit',
          relationship_end: 'Datum des Endes der Hochzeit',
          partner_avs: 'AHV-Nummer des Partners / der Partnerin',
          partner_death: 'Sterbedatum des Partners / der Partnerin',
          partner_birth: 'Geburtsdatum des Partners / der Partnerin',
          partner_age: 'Alter des Partners / der Partnerin',
          partner_gender: 'Geschlecht des Partners / der Partnerin',
          child: 'Kind',
          child_count: 'Anzahl der Kind(er)',
          child_birth: 'Geburtsdatum des Kindes',
          child_death: 'Sterbedatum des Kindes',
          child_is_alive: 'Lebend',
        },
      },
    },
    lpp: {
      video: {
        src: 'https://www.youtube.com/embed/n61WfLTW1IU?si=9DQllfCL3udT_xfp',
        title:
          'Das Drei-Säulen-Prinzip: Erklärendes Video vom Bundesamt für Sozialversicherung',
      },
      title:
        'Schätzung meiner Altersrente für Personaldienstleister (BVG – 2. Säule) <br />(BVG – 2. Säule)',
      action: 'Schätzung meiner Altersrente für Personaldienstleister',
      modal: {
        title: 'Ihre Pensionskasse ',
        content: `Jeden Januar erhalten Sie per Post ein Dokument namens "<strong>Vorsorgebescheinigung</strong>". Der Name Ihrer Pensionskasse ist darin angegeben. <br/>Ihr Arbeitgeber kann es Ihnen auf Anfrage ebenfalls zur Verfügung stellen`,
      },
      form: {
        label: 'Wählen Sie Ihre Pensionskasse/Vorsorgeeinrichtung aus',
      },
    },
    '3a': {
      video: {
        src: 'https://www.youtube.com/embed/lpiMQkzc_RE?si=A7Xf-pvD5bYb-pTI',
        title:
          'Das Drei-Säulen-Prinzip: Erklärendes Video vom Bundesamt für Sozialversicherung',
      },
      title: 'Schätzen der privaten Vorsorge <br />(3a – 3. Säule)',
      action: 'Schätzen der privaten Vorsorge',
      modal: {
        solde: {
          title: 'Aktueller Kontostand',
          content: `Diese Information finden Sie auf Ihrer jährlichen Steuerbescheinigung der 3. Säule, die Sie zwischen Ende Januar und Anfang Februar in Papierform erhalten. 
 <br/>
e-rentes.ch ist in der Lage, zwei Situationen der 3. Säule zu verarbeiten: 
<br/>
- 3a (gebundene Vorsorgekonto mit Anlage)<br/>
- 3a (gebundene Vorsorgekonto ohne Anlage)`,
        },
        amount: {
          title: 'Jährliche voraussichtliche Beiträge',
          content: `Hierbei handelt es sich um die jährlichen Beiträge, die Sie in Zukunft bis zum Rentenalter auf Ihr 3.
  Säule-Konto (3a) einzahlen möchten. <br/><br/> Wenn Sie angestellt sind,
  können Sie den maximal zulässigen Beitrag für das laufende Jahr abziehen, nämlich CHF 7'056.- (Höchstgrenze für 2023).
  Wenn Sie selbstständig sind und keinen 2. Säule-Pensionsplan haben, können Sie bis zu 20 % Ihres Einkommens abziehen,
  jedoch ohne die Obergrenze von CHF 35'280.- pro Jahr zu überschreiten (Höchstgrenze für 2023).`,
        },
        rate: {
          title: null,
          content: null,
        },
      },
      form: {
        solde: 'Aktueller Kontostand',
        amount: 'Höhe der prognostizierten jährlichen Beiträge',
        rate: 'Prognostizierter Zinssatz',
        paydate: 'Gewünschtes Datum für die Kapitalauszahlung',
      },
    },
    export: {
      title: 'e-rentes.ch: Schätzung der Altersrente',
      warning: `e-renten.ch ist ein Dienst, der es ermöglicht, online eine Schätzung von Renten und Alterskapital vorzunehmen. Das Ergebnis wird mithilfe vereinfachter Verfahren ermittelt und stellt keine Garantie dar. Da die Berechnung hypothetische Elemente enthält, handelt es sich bei den angegebenen Beträgen um Richtwerte. Diese Beträge haben daher keinen rechtlichen Wert und sind für die verschiedenen Organisationen, mit denen e-rentes.ch die Schätzungen austauscht, nicht bindend. Zudem begründet die Schätzung der Altersrenten und -kapitalien keinen Anspruch auf eine bestimmte Leistung.`,
    },
    synthesis: {
      title: 'Meine Daten',
      warning: `e-renten.ch ist ein Dienst, der es ermöglicht, online eine Schätzung von Renten und Alterskapital vorzunehmen. Das Ergebnis wird mithilfe vereinfachter Verfahren ermittelt und stellt keine Garantie dar. Da die Berechnung hypothetische Elemente enthält, handelt es sich bei den angegebenen Beträgen um Richtwerte. Diese Beträge haben daher keinen rechtlichen Wert und sind für die verschiedenen Organisationen, mit denen e-rentes.ch die Schätzungen austauscht, nicht bindend. Zudem begründet die Schätzung der Altersrenten und -kapitalien keinen Anspruch auf eine bestimmte Leistung.`,
      actions: {
        modify_personal_data: 'Meine staatliche Altersrente erneut schätzen',
        simulate_data_changes: 'Simulationen von Statusänderungen durchführen',
      },
      without_change: 'Ohne<br/> Änderung',
      with_change: 'Mit<br/> Änderung',
      parts: {
        total: {
          title: 'Renten & Kapital in CHF',
          annuity_label: 'Gesamte monatliche Renten',
          capital_label: 'Alterskapital',
        },
        lavs: {
          title: 'Staatliche Altersrente in CHF',
          from: 'von',
          to: 'bis',
          annuity_avs_label: 'Prognostizierte monatliche AVH-Rente',
          partner_annuity_from:
            'Ihr Ehepartner bezieht eine staatliche Altersrente ab dem',
        },
        lpp: {
          title: 'Berufliche Altersrente in CHF',
          action_label: 'Um Ihre Rente zu schätzen',
          action: 'Verbinden Sie Ihre berufliche Vorsorgekasse',
          annuity_label: 'Prognostizierte monatliche Rente',
        },
        '3a': {
          title: 'Private Vorsorge in CHF',
          action_label: 'Um Ihre private Vorsorge zu schätzen',
          action: 'Ihre Informationen hinzufügen',
          capital_label: 'Guthaben zu Ihren Gunsten',
          capital_until: 'Monatliche Zahlung bis zum',
          capital_projection_label: 'Prognostizierte Alterskapital',
          capital_projection_from: 'ab',
          capital_projection_to: 'am',
        },
      },
      modals: {
        simulate: {
          intro:
            'Jede Änderung der eingegebenen Informationen führt zu einer Anpassung der bereitgestellten Simulationen.',
          title: 'Änderungen der Daten',
          form: {
            salary: 'Bruttogehalt',
            rate: 'Aktivitätsrate',
            date: 'Renteneintrittsdatum',
            action: 'Meine Altersrenten simulieren',
            outro: `Was die AHV betrifft, ist es möglich, den Rentenbeginn um ein oder zwei Jahre vor dem gesetzlichen Rentenalter vorzuziehen oder umgekehrt um bis zu 5 Jahre zu verschieben.<br/><br/>

    Die gleiche Flexibilität gilt für die 2. Säule, jedoch hängt ihr Umfang von den Bestimmungen Ihrer Pensionskasse ab.`,
          },
        },
        total: {
          title: 'Renten- und Kapitalprognosen: Klarstellungen',
          content: `Die erste Teil der Zusammenfassung ist eine Zusammenfassung der folgenden drei Abschnitte und gibt die Schätzung an:
<br/>
- der monatlichen Rente der 1. Säule zusammen mit der monatlichen Rente der 2. Säule<br/>
- und des Kapitals der privaten Vorsorge (3a).`,
        },
        avs: {
          title: 'Die erforderlichen Informationen zur AHV',
          content: `Der angegebene Betrag entspricht einer Schätzung der monatlichen Rente, die Sie ab dem 65. Lebensjahr aus der 1. Säule erhalten werden. Beachten Sie jedoch, dass sich dieser Betrag ändern wird, sobald auch Ihr Ehepartner das Rentenalter erreicht hat, wenn Sie verheiratet sind.`,
        },
        lpp: {
          title:
            'Die erforderlichen Informationen zur beruflichen Vorsorge (BVG)',
          content: `Der angegebene Betrag entspricht einer Schätzung der monatlichen Rente, die Sie ab dem 65. Lebensjahr aus der 2. Säule erhalten werden.`,
        },
        '3a': {
          title: '3a Die erforderlichen Informationen',
          content: `e-rentes.ch kann zwei Arten der dritten Säule verarbeiten:
<br/>
- 3a (gebundene Vorsorgekonto mit Anlage)<br/>
- 3a (gebundene Vorsorgekonto ohne Anlage)`,
        },
      },
    },
  },
}
