export default {
  server: {
    port: process.env.SERVER_PORT || 4001, // par défaut: 3001
    host: process.env.SERVER_HOST || 'localhost', // par défaut: localhost
  },
  publicRuntimeConfig: {
    dibsApiBasePath: process.env.API_BASE_PATH,
    ver: process.env.npm_package_version
      ? process.env.npm_package_version
      : null,
  },

  // Disable server-side rendering: https://go.nuxtjs.dev/ssr-mode
  ssr: false,

  // Global page headers: https://go.nuxtjs.dev/config-head
  head: {
    title: 'e-rentes.ch',
    htmlAttrs: {
      lang: 'fr',
    },
    meta: [
      { charset: 'utf-8' },
      { name: 'viewport', content: 'width=device-width, initial-scale=1' },
      { hid: 'description', name: 'description', content: '' },
      { name: 'format-detection', content: 'telephone=no' },
    ],
  },

  // Global CSS: https://go.nuxtjs.dev/config-css
  css: [
    // CSS file in the project
    '@/node_modules/vue-loading-overlay/dist/vue-loading.css',
    '@typeform/embed/build/css/sidetab.css',
    '~/assets/css/main.css',
    '~/assets/css/components.css',
    '~/assets/css/form.css',
    '~/assets/css/synthesis.css',
    '~/assets/css/export.css',
    '~/assets/css/responsive.css',
    'vue-select/dist/vue-select.css',
  ],

  // Plugins to run before rendering page: https://go.nuxtjs.dev/config-plugins
  plugins: ['~/plugins/models.js', '~/plugins/api.js', '~/plugins/event-bus'],

  // Auto import components: https://go.nuxtjs.dev/config-components
  components: true,

  // Modules for dev and build (recommended): https://go.nuxtjs.dev/config-modules
  buildModules: [
    // https://go.nuxtjs.dev/eslint
    '@nuxtjs/eslint-module',
    '@nuxtjs/moment',
  ],
  moment: {
    locales: ['fr'],
  },

  // Modules: https://go.nuxtjs.dev/config-modules
  modules: ['@nuxtjs/axios', '@nuxtjs/pwa', '@nuxtjs/i18n'],
  i18n: {
    locales: [
      { code: 'fr', label: 'FR', iso: 'fr-CH', file: 'fr.js' },
      { code: 'de', label: 'DE', iso: 'de-CH', file: 'de.js', dir: 'ltr' },
    ],
    defaultLocale: 'fr',
    langDir: '~/locales/',
    vueI18n: {
      fallbackLocale: 'fr',
    },
  },

  // Axios module configuration: https://go.nuxtjs.dev/config-axios
  axios: {
    // Workaround to avoid enforcing hard-coded localhost:3000: https://github.com/nuxt-community/axios-module/issues/308
    baseURL: '/',
  },

  // PWA module configuration: https://go.nuxtjs.dev/pwa
  pwa: {
    manifest: {
      lang: 'fr',
    },
  },

  // Build Configuration: https://go.nuxtjs.dev/config-build
  build: {},
}
